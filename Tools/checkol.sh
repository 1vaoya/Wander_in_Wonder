#!/bin/zsh

if [[ "$#" = "0" ]]; then
	echo "pngがありません" 1>&2
	exit 1
fi

f1=$1
cp $f1 ${f1%.*}"!.png"

for f2 in $*; do
	if [ $f1 = $f2 ]; then
	else
		res=$(compare -metric AE $f1 $f2 diff.png 2>&1 > /dev/null)
		if [[ "$res" = "0" ]]; then
			cp $f2 ${f2%.*}"*.png"
		else 
			cp $f2 ${f2%.*}"!.png"
		fi
	fi
	f1=$f2;
done;

for f in $*; do
	rm -f $f
done

rm -f diff.png
