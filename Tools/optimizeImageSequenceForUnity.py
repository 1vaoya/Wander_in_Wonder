#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
import sys
import os
import glob
import cv2
import numpy as np
import json
import argparse

# メイン関数定義


def Main():

    # パーサーを作る
    parser = argparse.ArgumentParser(
        prog='optimizeImageSequenceForUnity.py',  # プログラム名
        usage='Trimming Images & Get Positions for Unity Animation',  # プログラムの利用方法
        description='description',  # 引数のヘルプの前に表示
        epilog='end',  # 引数のヘルプの後で表示
        add_help=True,  # -h/–help オプションの追加
    )

    parser.add_argument(
        '-l', '--landscape',
        action='store_true',
        help='Execute in landscape mode'
    )

    # parser.add_argument(
    #     '-inv', '--inverse',
    #     action='store_true',
    #     help='Execute in landscape mode'
    # )

    # 引数を解析する
    args = parser.parse_args()

    cwd = os.getcwd() + "/"
    print(cwd)

    if args.landscape:
        ProcessLandscape(cwd)
    else:
        ProcessAnimation(cwd)

    for folder in sorted(glob.glob("**/", recursive=True)):
        print(folder)
        if args.landscape:
            ProcessLandscape(folder)
        else:
            ProcessAnimation(folder)


def ProcessAnimation(folder):

    pathToImages = sorted(glob.glob(folder + "*.png"))

    if len(pathToImages) == 0:
        return

    datalist = None
    prevImg = None
    currentImg = None

    order = 0
    for path in pathToImages:

        filename = os.path.basename(path)
        filenameWithoutExt, ext = os.path.splitext(filename)
        data = {"name": filenameWithoutExt}
        prevImg = currentImg
        currentImg, x, y = Trimming(ReadImageFromPath(path))
        data.update({"x": x})
        data.update({"y": y})
        key = None

        if prevImg is None:
            key = True
        elif not(np.allclose(currentImg.shape, prevImg.shape)):
            key = True
        else:
            key = not(np.allclose(currentImg, prevImg))

        data.update({"key": key})

        if key:
            WriteImageToPath(folder, filename, currentImg)
        else:
            os.remove(path)

        data.update({"order": order})
        order+=1

        if datalist is None:
            datalist = [data]
        else:
            datalist.append(data)

    jsonfile = open(folder + "positions.json", "w")
    json.dump({"data": datalist}, jsonfile)


def ProcessLandscape(folder):
    ProcessAnimation(folder)


def ReadImageFromPath(path):
    return cv2.imread(path, cv2.IMREAD_UNCHANGED)


def WriteImageToPath(dir, filename, img):
    cv2.imwrite(dir + "/" + filename, img)


def Trimming(img):
    
    height, width = img.shape[:2]
    img, isBackground = MakeTranparentBlackPixels(img)
    
    if isBackground:
        print("Detect backgound image")
        return img, 0, 0

    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    ret, thresh = cv2.threshold(gray, 1, 255, cv2.THRESH_BINARY)
    out, contours, hierarchy = cv2.findContours(
        thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    allContourPoints = None

    for contourPoints in contours:
        if allContourPoints is None:
            allContourPoints = contourPoints
        else:
            allContourPoints = np.concatenate(
                [allContourPoints, contourPoints])

    x, y, w, h = cv2.boundingRect(allContourPoints)
    img = img[y:y + h, x:x + w]
    return img, (x + w / 2. - width / 2.), -(y + h / 2. - height / 2.)


def MakeTranparentBlackPixels(img):
    indexes = np.where(img[:, :, 3] == 0)
    zero = np.ones((len(indexes[1])), dtype="int8") * 0
    one = np.ones((len(indexes[1])), dtype="int8") * 1
    two = np.ones((len(indexes[1])), dtype="int8") * 2
    img[(indexes[0], indexes[1], zero)] = 0
    img[(indexes[0], indexes[1], one)] = 0
    img[(indexes[0], indexes[1], two)] = 0
    isBackground = (len(indexes[0]) == 0)
    return img, isBackground


# エントリポイント
Main()