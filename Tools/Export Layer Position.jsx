﻿/**
 * generator_png_layer_listup.jsx
 */

// 処理に該当する拡張子名の配列
// 今回は png のみなので .png
var extensions = [".png"];

// 以下処理 ///////////////////////////////////////////////////////
if (app.documents.length == 0) {
    alert("【！】実行するドキュメントがありません。");
} else {
    main();
}

function main() {
    if (confirm(activeDocument.name + "から、pngのつくレイヤーから矩形抽出しますか？")) {
        section();
    }
    alert("すべての処理が完了しました。お疲れ様でした。");
    return "完了";
}

function section() {

    // ファイルを作成し、テキストレイヤー書き出し処理へ
    var fileName = activeDocument.fullName + "_export.json";
    var file = new File(fileName);
    var openFlag = file.open("w");

    if (openFlag) {
        // 再帰的に処理
        var data = allLayerSetsBounds(activeDocument);
        // CSVテキストの出力
        file.write(WrapJSON(data));
        file.close();
        // 完了
        alert("書き出しが完了しました。");
    } else {
        alert("ファイルが開けませんでした。");
    }
}

// レイヤー名に処理に該当する拡張子が含まれているか捜索する
function checkNameExtensions(name) {
    var len = extensions.length;
    var ret = false;
    for (var i = 0; i < len; i++) {
        var checkName = extensions[i];
        if (name.indexOf(checkName) > -1) {
            ret = true;
        }
    }
    return true;
}

function getGeneratedLayersName(artLayers) {
    var ret = [];
    var ns = artLayers.length;
    for (var i = 0; i < ns; i++) {
        var focusLayer = artLayers[i];
        if (checkNameExtensions(focusLayer.name)) {
            ret.push(focusLayer.name);
        }
    }
    return ret;
}

function allLayerSetsBounds(layObj) {
    var strData = "";

    var n = layObj.artLayers.length;

    // レイヤーフォルダ内のレイヤー捜索 ////////////////////////////////////////

    // レイヤー名に処理に該当する拡張子が含まれているか捜索する
    var lList = getGeneratedLayersName(layObj.artLayers);
    // あれば1つ1つ捜索しテキスト生成しCSV用テキストへ連結
    var n = lList.length;
    if (n > 0) {
        $.writeln("[" + lList + "]");
        for (var i = 0; i < n; i++) {
            var lname = lList[i];
            var focusLayer = layObj.artLayers.getByName(lname);
            strData += CreateJSONLineFromLayer(focusLayer);  // CSV用テキストへ連結

            // 次にデータが続く場合はカンマを挿入
            if (i < n - 1)
            {
                strData += ",\n";
            }
        }
    }

    return strData;
}

function CreateCSVLine(name, x, y) {
    return name + "," + x + "," + y;
}

function CreateJSONLine(name, x, y) {
    var mName = "\"name\":" + "\"" + name + "\"";
    var mX = "\"x\":" + x;
    var mY = "\"y\":" + y;
    return "{\n" + mName + ",\n" + mX + ",\n" + mY + "\n}";
}

function CreateJSONLineFromLayer(layer) {
    var focusLayerArea = layer.bounds;  // フォーカスエリアを取得

    // レイヤーの中央座標を取得
    var xLeftUp = focusLayerArea[0];  // 左上 x座標
    var yLeftUp = focusLayerArea[1];  // 左上 y座標
    var xRightDown = focusLayerArea[2];  // 右下 x座標
    var yRightDown = focusLayerArea[3];  // 右下 y座標
    var xCenter = (xLeftUp + xRightDown) / 2;
    var yCenter = (yLeftUp + yRightDown) / 2;

    // Unity座標系に変換
    var widthCanvas = app.activeDocument.width.value;
    var heightCanvas = app.activeDocument.height.value;
    var xCanvasCenter = widthCanvas / 2;
    var yCanvasCenter = heightCanvas / 2;

    var xUnity = xCenter - xCanvasCenter;
    var yUnity = -(yCenter - yCanvasCenter);

    return CreateJSONLine(layer.name, xUnity.value, yUnity.value);
}

function WrapJSON(data) {
    return "{\"data\":[\n" + data + "\n]}";
}
