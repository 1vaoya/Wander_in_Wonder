// Upgrade NOTE: upgraded instancing buffer 'PerDrawSprite' to new syntax.

// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Sprites/ColorBurn"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        // my addition
        GrabPass { "_SharedGrabTexture" }

        Pass
        {
        CGPROGRAM
            #pragma vertex SpriteVert
            #pragma fragment SpriteFrag
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
            // Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

            #ifndef UNITY_SPRITES_INCLUDED
            #define UNITY_SPRITES_INCLUDED

            #include "UnityCG.cginc"

            #ifdef UNITY_INSTANCING_ENABLED

                UNITY_INSTANCING_BUFFER_START(PerDrawSprite)
                    // SpriteRenderer.Color while Non-Batched/Instanced.
                    fixed4 unity_SpriteRendererColorArray[UNITY_INSTANCED_ARRAY_SIZE];
                    // this could be smaller but that's how bit each entry is regardless of type
                    float4 unity_SpriteFlipArray[UNITY_INSTANCED_ARRAY_SIZE];
                UNITY_INSTANCING_BUFFER_END(PerDrawSprite)

                #define _RendererColor unity_SpriteRendererColorArray[unity_InstanceID]
                #define _Flip unity_SpriteFlipArray[unity_InstanceID]

            #endif // instancing

            CBUFFER_START(UnityPerDrawSprite)
            #ifndef UNITY_INSTANCING_ENABLED
                fixed4 _RendererColor;
                float4 _Flip;
            #endif
                float _EnableExternalAlpha;
            CBUFFER_END

            // Material Color.
            fixed4 _Color;

            // my addition
            sampler2D _SharedGrabTexture;

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            v2f SpriteVert(appdata_t IN)
            {
                v2f OUT;

                UNITY_SETUP_INSTANCE_ID (IN);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);

            #ifdef UNITY_INSTANCING_ENABLED
                IN.vertex.xy *= _Flip.xy;
            #endif

                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color * _Color * _RendererColor;

                #ifdef PIXELSNAP_ON
                OUT.vertex = UnityPixelSnap (OUT.vertex);
                #endif

                return OUT;
            }

            sampler2D _MainTex;
            sampler2D _AlphaTex;

            fixed4 SampleSpriteTexture (float2 uv)
            {
                fixed4 color = tex2D (_MainTex, uv);

            #if ETC1_EXTERNAL_ALPHA
                fixed4 alpha = tex2D (_AlphaTex, uv);
                color.a = lerp (color.a, alpha.r, _EnableExternalAlpha);
            #endif

                return color;
            }

            // my addition
            fixed4 ColorBurn (fixed4 a, fixed4 b)
      			{
      				fixed4 r = 1.0 - (1.0 - a) / b;
      				r.a = b.a;
      				return r;
      			}

            fixed4 SpriteFrag(v2f IN) : SV_Target
            {
                // my addition
                float2 grabTexcoord = IN.vertex.xy / IN.vertex.w;
                grabTexcoord.x = (grabTexcoord.x + 1.0) * .5;
                grabTexcoord.y = (grabTexcoord.y + 1.0) * .5;
                #if UNITY_UV_STARTS_AT_TOP
                grabTexcoord.y = 1.0 - grabTexcoord.y;
                #endif

                fixed4 grabColor = tex2D(_SharedGrabTexture, grabTexcoord);

                fixed4 c = SampleSpriteTexture (IN.texcoord) * IN.color;
                c.rgb *= c.a;
                return ColorBurn(grabColor, c);

                // defalut sprite shader
                //fixed4 c = SampleSpriteTexture (IN.texcoord) * IN.color;
                //c.rgb *= c.a;
                //return c;

                // http://www.shibuya24.info/entry/2016/10/26/003039?amp=1
                //half4 color = (tex2D(_MainTex, IN.texcoord) + _TextureSampleAdd);
                //color.rgb += IN.color.rgb;
                //color.a *= IN.color.a;
                //color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);

                //#ifdef UNITY_UI_ALPHACLIP
                //clip (color.a - 0.001);
                //#endif

                //return color;
            }

            #endif // UNITY_SPRITES_INCLUDED
        ENDCG
        }
    }
}
