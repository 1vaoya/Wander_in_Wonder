﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LR {
    L,
    R
}

public enum LookDir {
    front,
    back,
    right,
    left
}

public enum MoveType {
    walk,
    trot,
    run,
    jump,
    down,
    custom
}

public class AnimationChanger : MonoBehaviour {


    protected Animator animator;
    protected SpriteRenderer renderer;
    [SerializeField] Transform pivot;

    public bool IsMovable {
        get { return !IsBusy && isMovable; }
    }

    public bool IsBusy {
        get { return isTurning.Value || isSearching.Value; }
    }
    public bool isMovable = false;

    [SerializeField]
    public Trigger isTurning ,isSearching;
    Trigger rotate;

    LR lr = LR.R;

    public Vector2 initDir = new Vector2(1f, 0.4f);
    public bool initLeft = false;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        renderer = GetComponent<SpriteRenderer>();
        isTurning = new Trigger();
        isSearching = new Trigger();
        rotate = new Trigger();

        if (initLeft) {
            lr = LR.L;
            renderer.flipX = true;
        }
        RotateTo(initDir);
	}

    // Update is called once per frame
    void FixedUpdate() {
        if (rotate.Value && !IsBusy) {
            RotateTo(_dir);
        }
    }

    public void StartMove () {
        if (IsBusy) {
            //Debug.LogError("isSearching " + isSearching.Value + " isTurning " + isTurning.Value);
            return;
        }
        animator.SetTrigger("move");
        isSearching.Activate();
	    //Debug.LogError("2. Start Move & Search Activate, moveType is" + animator.GetInteger("moveType"));
    }

    public void SetMoveDir(Vector2 dir)
    {
        if (IsBusy) return;

        LR _lr = (dir.x > 0 ? LR.R : LR.L);
        if (lr != _lr) {
            Turn();
        }

        lr = _lr;
        _dir = dir;
        rotate.Activate();
    }

    public void SetMoveType (MoveType type) {
        if (IsBusy) return;
	    //Debug.LogError("1. Set MoveType, before " + animator.GetInteger("moveType").ToString() + " after " + type.ToString());
        switch (type) {
            case MoveType.walk:
                animator.SetInteger("moveType", 1);
                break;
            case MoveType.trot:
                animator.SetInteger("moveType", 2);
                break;
            case MoveType.run:
                animator.SetInteger("moveType", 3);
                break;
            case MoveType.jump:
                animator.SetInteger("moveType", 4);
                break;
            case MoveType.down:
                animator.SetInteger("moveType", 5);
                break;
        }
    }

    [SerializeField] public Vector2 _dir;
    void RotateTo (Vector2 dir) {
        //float diffAngle = Vector3.Angle((renderer.flipX ? -1 : 1) * animator.transform.right, dir);
        float diffAngle = Vector3.Angle((lr == LR.L ? -1 : 1) * animator.transform.right, dir);
        bool migineji = Vector3.Dot(Vector3.Cross((lr == LR.L? -1 : 1) * animator.transform.right, dir), -Vector3.forward) > 0;
        animator.transform.RotateAround(pivot.position, Vector3.forward, (migineji ? -1 : 1) * diffAngle);
    }

    public void SearchAnimation (Vector2 vec) {
        bool left = !renderer.flipX;
        bool X, Y;
        X = (vec.x >= 0);
        Y = (vec.y >= 0);
        bool temp = (Mathf.Abs(vec.x) > Mathf.Abs(vec.y));

        LookDir dir = LookDir.front;

        if (X && Y) {
            //第一象限
            if (temp) {
                dir = LookDir.front;
            } else {
                dir = LookDir.left;
            }
        } else if (!X && Y) {
            //第二象限
            if (temp) {
                dir = LookDir.back;
            } else {
                dir = LookDir.left;
            }
        } else if (!X && !Y) {
            //第三象限
            if (temp) {
                dir = LookDir.back;
            } else {
                dir = LookDir.right;
            }
        } else if (X && !Y) {
            //第四象限
            if (temp) {
                dir = LookDir.front;
            } else {
                dir = LookDir.right;
            }
        }

        switch (dir) {
            case LookDir.front:
                if (left) {
                    //animator.SetTrigger("toFront");
                    animator.SetInteger("lookDir", 1);
                } else {
                    //animator.SetTrigger("toBack");
                    animator.SetInteger("lookDir", 4);
                }
                break;
            case LookDir.back:
                if (left) {
                    //animator.SetTrigger("toBack");
                    animator.SetInteger("lookDir", 4);
                } else {
                    //animator.SetTrigger("toFront");
                    animator.SetInteger("lookDir", 1);
                }
                break;
            case LookDir.left:
                //animator.SetTrigger("toLeft");
                animator.SetInteger("lookDir", 2);
                break;
            case LookDir.right:
                //animator.SetTrigger("toRight");
                animator.SetInteger("lookDir", 3);
                break;
        }
    }

    public void StopMove () {
        if (IsBusy) return;
        animator.SetInteger("moveType", 0);
        animator.SetTrigger("exit");
	    //Debug.LogError("4. Stop Move, moveType is" + animator.GetInteger("moveType"));
    }

    void SetLookDir (LookDir dir) {
        if (IsBusy) return;
        switch (dir) {
            case LookDir.front:
                animator.SetInteger("lookDir", 1);
                break;
            case LookDir.left:
                animator.SetInteger("lookDir", 2);
                break;
            case LookDir.right:
                animator.SetInteger("lookDir", 3);
                break;
            case LookDir.back:
                animator.SetInteger("lookDir", 4);
                break;
        }
    }

    void Turn () {
        if (IsBusy) return;
        animator.SetBool("turn", true);
        //isTurning = true;
        Debug.LogError("SetBool(\"turn\", true)");
    }

    public void Flip () {
        renderer.flipX = !renderer.flipX;
    }

    //void Flipper () {
    //    Trigger.Change ch = TurnTrigger.DetectChange(isTurning);
    //    if (ch != Trigger.Change.none) Debug.Log(ch);
    //    if (ch == Trigger.Change.fall) {
    //        renderer.flipX = (lr == LR.L ? true : false);
    //        //Debug.Log("flipped");
    //    }
    //}

    public void IdleChanger () {
        int idleType = Random.Range(1, 5);
        animator.SetInteger("idleType", idleType);
    }

    public void SetMovable (int movable) {
        isMovable = movable == 0 ? false : true;
    }

    public void SetLayer (string layer, int order) {
        renderer.sortingOrder = order;
        renderer.sortingLayerName = layer;
    }

    public void SetSize (float size) {
        transform.localScale = size * Vector3.one;
    }

    public void CompleteTurn () {
        animator.SetBool("turn", false);
    }

    public void ForceTurn (float dir) {
      //RotateTo(new Vector2(1, 0));
      renderer.flipX = false;
      lr  = LR.R;
      _dir = new Vector2(Mathf.Cos(Mathf.Deg2Rad * dir), Mathf.Sin(Mathf.Deg2Rad * dir));
      RotateTo(_dir);
    }
}
