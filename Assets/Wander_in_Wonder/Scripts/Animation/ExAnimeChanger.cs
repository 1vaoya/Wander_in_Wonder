﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExAnimeChanger : MonoBehaviour {
    MoveType type;
    public Agent agent;
    public AnimationChanger anime;
    public SpriteRenderer renderer;
    public Animator demo;

    public bool naname = false;
    public bool dance = false;

    void Update()
    {
        if (agent.IsMovable && agent.GetEdge() != null && !agent.GetEdge().isRotatable)
        {
            GetComponent<SpriteRenderer>().flipX = renderer.flipX;
            if (agent.GetEdge().up)
            {
                renderer.enabled = false;
                GetComponent<Animator>().SetInteger("naname", 1);
            }
            else
            {
                renderer.enabled = false;
                GetComponent<Animator>().SetInteger("naname", -1);
            }
            naname = true;
        }
        else
        {
            renderer.enabled = true;
            GetComponent<Animator>().SetInteger("naname", 0);
            naname = false;
        }

        GetComponent<SpriteRenderer>().enabled = true;
        if (GetComponent<Animator>().GetInteger("naname") == 0 && GetComponent<Animator>().GetBool("dance"))
        {
            if (agent.state == Agent.State.move || demo.GetInteger("demo") != 0)
            {
                GetComponent<SpriteRenderer>().enabled = false;
                dance = false;
            }
            else
            {
                GetComponent<SpriteRenderer>().enabled = true;
                dance = true;
            }
        }
        GetComponent<SpriteRenderer>().sortingLayerName = renderer.sortingLayerName;
        GetComponent<SpriteRenderer>().sortingOrder = renderer.sortingOrder;
        if (!dance) transform.localScale = renderer.transform.localScale * 1.4f;
        else transform.localScale = renderer.transform.localScale * 1.3f;
    }
}
