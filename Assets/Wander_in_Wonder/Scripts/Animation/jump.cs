﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jump : StateMachineBehaviour {

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //if (animator.GetComponent<DemoManager>() != null) animator.gameObject.GetComponent<DemoManager>().agent.enabled = false;
        //else if (animator.GetComponent<Demo6>() != null)
        //{
        //   var agent = animator.GetComponent<Demo6>().agent;
        //    agent.enabled = false;
        //}
        if (animator.GetComponent<DemoManager>() != null)
        {
            var agent = animator.gameObject.GetComponent<DemoManager>().agent;
            agent.JumpToNext();
            animator.gameObject.GetComponent<DemoManager>().agent.enabled = false;
        }
        else if (animator.GetComponent<Demo6>() != null)
        {
            var agent = animator.GetComponent<Demo6>().agent;
            agent.JumpToNext();
            animator.gameObject.GetComponent<Demo6>().agent.enabled = false;
        }
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //if (animator.GetComponent<DemoManager>() != null)
        //{
        //    var agent = animator.gameObject.GetComponent<DemoManager>().agent;
        //    agent.JumpToNext();
        //    animator.gameObject.GetComponent<DemoManager>().agent.enabled = true;
        //} else if (animator.GetComponent<Demo6>() != null) {
        //    var agent = animator.GetComponent<Demo6>().agent;
        //    agent.JumpToNext();
        //    animator.gameObject.GetComponent<Demo6>().agent.enabled = true;
        //}
        if (animator.GetComponent<DemoManager>() != null) animator.gameObject.GetComponent<DemoManager>().agent.enabled = true;
         else if (animator.GetComponent<Demo6>() != null)
         {
            var agent = animator.GetComponent<Demo6>().agent;
             agent.enabled = true;
         }
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
