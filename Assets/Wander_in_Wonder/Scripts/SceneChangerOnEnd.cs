﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangerOnEnd : MyEvent {

    [SerializeField] string scene;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
    }

    public override void OnEventStart()
    {
    }

    public override void OnEventStay()
    {

    }

    public override void OnEventEnd()
    {
        StartCoroutine("Change");
    }

    public IEnumerator Change () {
        for (int i = 0; i < 30; i++) {
            yield return null;
        }
        SceneManager.LoadScene(scene);
        Resources.UnloadUnusedAssets();
        System.GC.Collect();
    }
}
