using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GodTouches;

public class TouchManager : SingletonMonoBehaviour<TouchManager> {

    [SerializeField] GameObject tap;

    // Use this for initialization
    void Start () {
    }

    // Update is called once per frame
    void Update () {
        switch (GodTouch.GetPhase()) {
            case GodPhase.Began: {
                    Vector2 tappedPoint = Camera.main.ScreenToWorldPoint(GodTouch.GetPosition());
                    RaycastHit2D hit = Physics2D.Raycast(tappedPoint, -Vector2.zero);
                    if (hit.collider != null && hit.collider.gameObject.GetComponent<TappedObject>() != null) {
                        TappedObject tappedObj = hit.collider.gameObject.GetComponent<TappedObject>();
                        tappedObj.OnTapBegan(tappedPoint);
                        Instantiate(tap, tappedPoint, Quaternion.identity);
                    } else {
                        
                    }
                    break;
                }
            case GodPhase.Moved:
                break;
            case GodPhase.Ended:
                break;
        }
    }
}
