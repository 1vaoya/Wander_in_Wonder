using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface TappedObject {

	void OnTapBegan (Vector2 position);
	void OnTapMoved (Vector2 position, Vector2 deltaPosition);
	void OnTapEnded (Vector2 position);

}
