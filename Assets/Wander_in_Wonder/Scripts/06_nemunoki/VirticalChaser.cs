﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirticalChaser : MonoBehaviour {

    [SerializeField] Transform target;
    [SerializeField] float limitDown, limitUp;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var p = target.position.y;

        if (p < limitDown)
        {
            p = limitDown;
        }
        else if (limitUp < p)
        {
            p = limitUp;
        }

        transform.position += 0.05f * (new Vector3(transform.position.x, p, transform.position.z) - transform.position);
    }
}
