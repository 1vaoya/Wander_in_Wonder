﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nemunoki : MyEvent, TappedObject {

    public enum State {
        sleep,
        awake
    }

    bool isMorfing = false;
    public State state;
    public Agent agent;
    public Node from, to;
    public bool isPassed = false;
    public int wakeTime = 3;
    public Animator demo;
    public int demoId = 2;
    public bool isDemo = false;

    // Use this for initialization
	void Start () {
        state = State.awake;
	}


    // Update is called once per frame
    int counter = 0;
	void FixedUpdate () {
        if (state == State.sleep) {
            counter++;
            if ((agent.GetEdge() != GraphManager.Instance.GetEdge(from, to) && demo.GetInteger("demo") != demoId) && (counter >= wakeTime * 60 || isPassed) && !isMorfing) {
                WakeUp();
            }
        }
        if (agent.currentNode == to) {
            isPassed = true;
        }
	}

    void Sleep () {
        //GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<Animator>().SetTrigger("sleep");
        GraphManager.Instance.GetEdge(from, to).isPassable = true;
        counter = 0;
        isMorfing = true;
    }

    void AfterSleep () {
        state = State.sleep;
        isMorfing = false;
    }

    void WakeUp () {
        //GetComponent<BoxCollider2D>().enabled = true;
        GetComponent<Animator>().SetTrigger("wake");
        GraphManager.Instance.GetEdge(from, to).isPassable = false;
        isMorfing = true;
    }

    void AfterWakeUp () {
        state = State.awake;
        isMorfing = false;
    }

    public void OnTapBegan(Vector2 position)
    {
        if (state == State.awake) {
            if (!isMorfing) {
                Sleep();
                GetComponent<AudioSource>().Play();
            }
        } else if (state == State.sleep) {

            GetComponents<AudioSource>()[1].Play();
            if (agent.currentNode == from) {
                agent.StartMove(to);
            } else if (!isPassed){
                agent.StartMove(from);
            }
        }
    }

    public void OnTapMoved(Vector2 position, Vector2 deltaPosition)
    {

    }

    public void OnTapEnded(Vector2 position)
    {

    }

    public override void OnEventStart()
    {
        demo.SetInteger("demo", demoId);
        if (isDemo) {
            GetComponent<Animator>().SetTrigger("demo");
        }
    }

    public override void OnEventStay()
    {

    }

    public override void OnEventEnd()
    {

    }
}
