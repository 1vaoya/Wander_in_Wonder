﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Demo6 : MyEvent {

    public Animator animator;
    public Agent agent;
    public AnimationChanger animeChanger;

    public Animator shadow;

    public Node outnode, joyFrom, joyTo;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetInteger("demo", 1);
        shadow.SetTrigger("shadow");
    }

    bool joyed = false;
    void Update() {
        if (animator.GetInteger("demo") == 0) {
            GetComponent<SpriteRenderer>().sortingLayerID = animeChanger.GetComponent<SpriteRenderer>().sortingLayerID;
            GetComponent<SpriteRenderer>().sortingOrder = animeChanger.GetComponent<SpriteRenderer>().sortingOrder;
        }
        if (agent.currentNode == outnode) {
            animator.SetInteger("demo", 6);
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (animator.GetInteger("demo") != 0)
        {
            animeChanger.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        } else {
            animeChanger.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    public void SetLayer (string name) {
        GetComponent<SpriteRenderer>().sortingLayerName = name;
        animeChanger.SetLayer(name, animeChanger.GetComponent<SpriteRenderer>().sortingOrder);
    }

    public void SetOrder (int order) {
      GetComponent<SpriteRenderer>().sortingOrder = order;
      animeChanger.SetLayer(animeChanger.GetComponent<SpriteRenderer>().sortingLayerName, order);
    }

    public void NextScene () {
        SceneManager.LoadScene("06to07");
    }

    public void ForceTurn (float dir) {
      animeChanger.ForceTurn(dir);
    }

    public override void OnEventStart () {
        if (!joyed)
        {
            agent.FinishMove();
        }
    }
    public override void OnEventStay () {
        
    }
    public override void OnEventEnd () {
        if (!joyed)
        {
            joyed = true;
            animator.SetInteger("demo", 4);
        }
    }
}
