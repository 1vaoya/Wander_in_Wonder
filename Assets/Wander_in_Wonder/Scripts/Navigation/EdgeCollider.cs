﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeCollider : MonoBehaviour, TappedObject {

	public float width = 2;
	public Node nodeA, nodeB;

	Vector2 vectorAB, vectorBA;

	// Use this for initialization
	void Start () {
		if (nodeA != null && nodeB != null) {
			Init(nodeA, nodeB);
		}
	}

	// Update is called once per frame
	void Update () {

	}

	public void Init (Node nodeA, Node nodeB) {
		this.nodeA = nodeA;
		this.nodeB = nodeB;

		vectorAB = nodeB.position - nodeA.position;
		vectorBA = -vectorAB;

		transform.localPosition = (nodeA.position + nodeB.position) / 2;
		transform.rotation = Quaternion.FromToRotation(Vector2.right, nodeB.position - nodeA.position);
		transform.localScale = new Vector2(vectorAB.magnitude, width);
	}

	public void OnTapBegan (Vector2 position) {
        GraphManager.Instance.targetNode = SnapToNode(position);
        GraphManager.Instance.isUpdated = true;
        var audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(audioSource.clip);
	}

	public void OnTapMoved (Vector2 position, Vector2 deltaPosition) {

	}

	public void OnTapEnded (Vector2 position) {

	}

	public Vector2 ProjectToEdge (Vector2 position) {
		Vector2 vectorPA = nodeA.position - position;
		float ratio = -Vector2.Dot(vectorAB, vectorPA) / Mathf.Pow(vectorAB.magnitude, 2);

		return nodeA.position + ratio * vectorAB;
	}

    public Node SnapToNode (Vector2 position) {
        float distToA, distToB;
        distToA = GetDistanceToNode(position, nodeA);
        distToB = GetDistanceToNode(position, nodeB);
        return distToA < distToB ? nodeA : nodeB;
    }

    float GetDistanceToNode (Vector2 position, Node node) {
        return Mathf.Sqrt(Mathf.Pow(position.x - node.position.x, 2) + Mathf.Pow(position.y - node.position.y, 2));
    }
}
