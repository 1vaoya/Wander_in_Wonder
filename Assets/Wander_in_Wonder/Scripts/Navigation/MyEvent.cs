﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyEvent : MonoBehaviour {

    public virtual void OnEventStart () {
        
    }
    public virtual void OnEventStay () {
        
    }
    public virtual void OnEventEnd () {
        
    }
}
