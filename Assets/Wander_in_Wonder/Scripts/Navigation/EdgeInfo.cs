﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EdgeInfo {
    public Node connectedNode;
    public MyEvent edgeEvent;
    public bool isCollider = true;
    public bool isPassable = true;
    public MoveType type = MoveType.walk;
    public bool isRotatable = true;
}