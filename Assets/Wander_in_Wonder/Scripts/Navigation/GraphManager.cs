using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphManager : SingletonMonoBehaviour<GraphManager> {
    
    [SerializeField] bool isVisible;

    public Node startNode, endNode;

    [SerializeField] GameObject edgePrefab;

    public bool isUpdated = false;
    public Node targetNode;
    public Vector2 targetPosition {
        get { return targetNode != null ? targetNode.position : Vector2.zero; }
    }

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void FixedUpdate () {
        
    }

    public void BuildGraph () {
        // 古いエッジの削除
        var es = GetComponentsInChildren<EdgeCollider>();
        foreach (var e in es) {
            if (e != null) DestroyImmediate(e.gameObject);
        }

        // 新しいエッジの生成
        var nodes = GetComponentsInChildren<Node>();
        for (int A = 0; A < nodes.Length; A++) {
            for (int B = A + 1; B < nodes.Length; B++) {
                EdgeInfo edgeFromAtoB = GetEdgeInfo(nodes[A], nodes[B]);
                EdgeInfo edgeFromBtoA = GetEdgeInfo(nodes[B], nodes[A]);

                if (edgeFromAtoB != null && edgeFromBtoA != null && (edgeFromAtoB.isCollider != edgeFromBtoA.isCollider)) {
                    Debug.LogError(nodes[A].name + "と" + nodes[B].name + "のコライダ定義が一致しません");
                }

                if ((edgeFromAtoB != null ? edgeFromAtoB.isCollider : false) || (edgeFromBtoA != null ? edgeFromBtoA.isCollider : false)) {
                    GameObject edge = Instantiate(edgePrefab, transform);
                    edge.transform.position = transform.position;
                    edge.GetComponent<EdgeCollider>().Init(nodes[A], nodes[B]);
                }
            }
        }

        // 可視化の切り替え
        var meshRenderers = GetComponentsInChildren<MeshRenderer>();
        foreach (var meshRenderer in meshRenderers) {
            meshRenderer.enabled = isVisible;
        }
    }

    EdgeInfo GetEdgeInfo (Node from, Node to) {
        EdgeInfo info = null;
        foreach (var edgeInfo in from.edgeInfo) {
            if  (edgeInfo.connectedNode.Equals(to)) {
                info = edgeInfo;
            }
        }
        return info;
    }

    public Edge GetEdge (Node from, Node to) {
        Edge edge = null;
        foreach (var e in from.edges) {
            if (e.to.Equals(to)) {
                edge = e;
            }
        }
        return edge;
    }
}
