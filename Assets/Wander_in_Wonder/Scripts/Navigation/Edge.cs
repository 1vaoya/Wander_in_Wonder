﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edge {
    public Node from, to;
    public bool isPassable;
    public MoveType type;
    public float length {
        get { return (from.position - to.position).magnitude; }
    }
    public Edge (Node _from, Node _to, bool _isPassable = true) {
        from = _from;
        to = _to;
        isPassable = _isPassable;
    }
    public MyEvent edgeEvent;
    public bool isRotatable;
    public Vector2 dir {
        get {
            if (isRotatable) return (to.position - from.position);
            else return ((to.position - from.position).x > 0 ? 1 : -1) * new Vector2(1, 0);
        }
    }
    public bool up {
        get {
            return (to.position - from.position).y >= 0;
        }
    }
}
