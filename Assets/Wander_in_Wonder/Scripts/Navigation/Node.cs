﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour, TappedObject {

	public Vector2 position {
		get {return transform.position;}
		set {transform.position = value;}
	}

    public EdgeInfo[] edgeInfo;
    public List<Edge> edges;
    public string sortingLayer = "01";
    public int orderInLayer = 0;
    public float size = 1;

	// Use this for initialization
	void Start () {
        edges = new List<Edge>();
        foreach (EdgeInfo info in edgeInfo) {
            var e = new Edge(this, info.connectedNode, info.isPassable);
            edges.Add(e);
            e.edgeEvent = info.edgeEvent;
            e.type = info.type;
            e.isRotatable = info.isRotatable;
        }
	}

	// Update is called once per frame
	void Update () {

	}

	public void OnTapBegan (Vector2 position) {
		GraphManager.Instance.targetNode = this;
        GraphManager.Instance.isUpdated = true;
        var audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(audioSource.clip);
	}

	public void OnTapMoved (Vector2 position, Vector2 deltaPosition) {

	}

	public void OnTapEnded (Vector2 position) {

	}
}
