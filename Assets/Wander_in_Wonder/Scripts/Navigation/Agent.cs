﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Agent : MonoBehaviour
{

    public enum State
    {
        idle,
        move
    }

    GraphManager graphMngr;
    public Animator usagiAnimator;

    public Node frameout, frameout2;

    [Header("State")]
    public Node currentNode;

    [SerializeField] State _state = State.idle;
    public State state
    {
        get { return _state; }
        private set { _state = value; }
    }

    [SerializeField] bool isMovable = true;
    public bool IsMovable
    {
        get { return isMovable; }
        set { isMovable = value; }
    }

    [Header("Pram")]
    [SerializeField]
    float threshold = 0.1f;
    float speed = 0.1f;
    public float Speed
    {
        get { return speed; }
        set { speed = value; }
    }

    [Header("MoveStart")]
    [SerializeField]
    private UnityEngine.Events.UnityEvent m_start = new UnityEngine.Events.UnityEvent();
    [Header("Moving")]
    [SerializeField]
    private UnityEngine.Events.UnityEvent m_stay = new UnityEngine.Events.UnityEvent();
    [Header("MoveToNext")]
    [SerializeField]
    private UnityEngine.Events.UnityEvent m_next = new UnityEngine.Events.UnityEvent();
    [Header("MoveEnd")]
    [SerializeField]
    private UnityEngine.Events.UnityEvent m_end = new UnityEngine.Events.UnityEvent();


    Node nextNode, destinationNode;
    Stack<Node> path;
    Vector2 moveDir = Vector2.zero;
    Trigger startTrigger = new Trigger();

    // Use this for initialization
    void Start()
    {
        //animeChanger = GetComponentInChildren<AnimationChanger>();

        graphMngr = GraphManager.Instance;

        SnapToNode(currentNode);
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //if (!IsMovable) return;

        switch (state)
        {
            case State.idle:

                if (startTrigger.Value)
                {
                    Debug.LogError("Agent start move");
                    if (GetComponent<Usagi>().anime.IsBusy)
                    {
                        Debug.LogError("startTrigger is true but anime is *busy*");
                        FinishMove();
                    }
                    else
                    {
                        path = Navigation.GetPath(currentNode, destinationNode);
                        currentNode = path.Pop();
                        if (SetNextNode(path.Pop()))
                        {
                            state = State.move;
                        }

                        //Debug.LogError("1.2. StartTrigger Done");
                        m_start.Invoke();
                        IsMovable = false;

                    }
                    startTrigger.Complete();
                }

                break;

            case State.move:

                if (IsOnNode(nextNode))
                {
                    currentNode = nextNode;
                    if (path.Count == 0 || !SetNextNode(path.Pop()))
                    {
                        m_end.Invoke();
                        state = State.idle;
                        destinationNode = currentNode;
                        path.Clear();
                        if (endEvent != null) endEvent.OnEventEnd();
                        break;
                    }
                }

                if (IsMovable) Walk();

                break;
        }

    }

    bool IsOnNode(Node node)
    {
        if (node == null) return false;
        float dist = (node.position - (Vector2)transform.position).magnitude;
        bool ans = dist < threshold;
        if (ans) SnapToNode(node);
        return ans;
    }

    void Walk()
    {
        transform.position += (Vector3)moveDir * speed;
        m_stay.Invoke();
    }

    void SnapToNode(Node node)
    {
        transform.position = node.position;
    }

    bool SetNextNode(Node next)
    {
        if (!graphMngr.GetEdge(currentNode, next).isPassable)
        {
            return false;
        }

        nextNode = next;

        moveDir = (nextNode.position - currentNode.position).normalized;

        var e = graphMngr.GetEdge(currentNode, nextNode).edgeEvent;
        if (e != null) e.OnEventStart();

        m_next.Invoke();

        return true;
    }

    // 次のノードについたときに強制的に移動を中止させる処理
    MyEvent endEvent;
    public void FinishMove()
    {
        endEvent = graphMngr.GetEdge(currentNode, nextNode).edgeEvent;
        destinationNode = nextNode;
        path.Clear();
    }

    public bool StartMove(Node destination)
    {
        // 移動を開始できないときの処理
        if (destinationNode == destination)
        {
            // 目的地が一緒のときはなにもせずに無視。そのまま動き続けるか、止まり続ける。
            return false;
        }
        if (!Navigation.IsConnected(currentNode, destination))
        {
            // 目的地のノードと現在のノードが繋がっていないときも無視。
            if (usagiAnimator != null && state == State.idle)
            {
                usagiAnimator.SetTrigger("cantGo");
            }
            Debug.Log("目標ノードに到達できません");
            return false;
        }
        if (state == State.move && destinationNode != destination)
        {
            // 移動中に現在の目的地と違う目的地を指定された場合は移動を中断して待機に入る
            FinishMove();
            return false;
        }
        if (destination == currentNode)
        {
            return false;
        }


        Debug.LogError("Prepare Move not busy");

        // 普通の移動開始の処理
        startTrigger.Activate();
        //Debug.LogError("1.1. StartTrigger ON");
        destinationNode = destination;
        if (destination == frameout)
        {
            destinationNode = frameout2;
        }
        path = Navigation.GetPath(currentNode, destinationNode);
        currentNode = path.Pop();
        nextNode = path.Pop();

        return true;
    }

    public Edge GetEdge()
    {
        return graphMngr.GetEdge(currentNode, nextNode);
    }

    public void JumpToNext()
    {
        transform.position = nextNode.position;
    }

    public void WarpTo(Node node)
    {
        if (state == State.idle)
        {
            currentNode = node;
            SnapToNode(node);
        }
        else
        {
            Debug.Log("移動中だからワープできんよ");
        }
    }
}
