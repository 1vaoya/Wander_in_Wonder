﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Navigation {

    static public bool IsConnected (Node from, Node to) {
        var que = new Queue<Node>();
        var prev = new Dictionary<Node, Node>();

        que.Enqueue(from);
        prev.Add(from, null);

        bool isConnected = false;

        while (que.Count != 0) {
            Node item = que.Dequeue();
            var edges = item.edges;
            foreach (var edge in edges) {
                if (!prev.ContainsKey(edge.to) && edge.isPassable) {
                    que.Enqueue(edge.to);
                    prev.Add(edge.to, edge.from);
                    if (edge.to.Equals(to))
                    {
                        isConnected = true;
                        break;
                    }
                }
            }
        }

        return isConnected;
    }

    static public Stack<Node> GetPath (Node from, Node to) {
        var que = new Queue<Node>();
        var prev = new Dictionary<Node, Node>();

        que.Enqueue(from);
        prev.Add(from, null);

        while (que.Count != 0) {
            Node item = que.Dequeue();
            var edges = item.edges;
            foreach (var edge in edges) {
                if (!edge.isPassable) {
                    Debug.Log("is not passible");
                    Debug.Log(edge.from);
                    Debug.Log(edge.to);
                }
                if (!prev.ContainsKey(edge.to) && edge.isPassable) {
                    que.Enqueue(edge.to);
                    prev.Add(edge.to, edge.from);
                    if (edge.to.Equals(to)) break;
                }
            }
        }

        var path = new Stack<Node>();
        Node node = to;
        path.Push(node);
        while (prev[node] != null) {
            node = prev[node];
            path.Push(node);
        }

        return path;
    }
}
