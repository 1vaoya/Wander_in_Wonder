﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapGuide : MonoBehaviour {

	public Agent agent;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (agent.state == Agent.State.move) {
			GetComponent<Animator>().SetTrigger("End");
		}
	}
}
