﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Onamomi : MyEvent, TappedObject{

    public enum OnamomiState {
        idle,
        stick,
        removed
    }

    Animator animator;
    public Transform usagi;
    public OnamomiState state = OnamomiState.idle;
    public Node node;
    public Transform pivot;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();   
	}
	
	// Update is called once per frame
	void Update () {
     
	}

    public void StartStick () {
        if (state == OnamomiState.idle) {
            animator.SetTrigger("stick");
            state = OnamomiState.stick;
        }
    }

    public void EnableCollider () {
        var c = GetComponent<CircleCollider2D>();
        c.enabled = true;
    }

    public override void OnEventStart () {
        //if (state != OnamomiState.idle) return;
        //transform.parent = usagi;
        //transform.position += pivot.position;
        //state = OnamomiState.stick;
        //foreach (var e in node.edges) {
        //    e.isPassable = false;
        //}
    }

    public override void OnEventStay () {
        
    }

    public override void OnEventEnd () {
        
    }

    public void OnTapBegan (Vector2 position) {
        if (state == OnamomiState.stick) {
            state = OnamomiState.removed;
            animator.SetTrigger("remove");
        }
    }

    public void OnTapMoved (Vector2 position, Vector2 deltaPosition) {
        
    }

    public void OnTapEnded (Vector2 position){
        
    }

    public void Removed () {
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<CircleCollider2D>().enabled = false;
    }

    public void Play()
    {
        GetComponent<AudioSource>().Play();
    }
}
