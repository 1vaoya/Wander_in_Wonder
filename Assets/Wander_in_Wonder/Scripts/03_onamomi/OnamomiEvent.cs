﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnamomiEvent : MyEvent
{

    public OnamomiManager oMgr;
    public DemoManager demo;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        demo.animator.SetBool("onamomi", oMgr.state == OnamomiManager.OnamomiState.stick);
    }

    public override void OnEventStart()
    {
        demo.animator.SetInteger("demo", 6);
        demo.animator.SetBool("onamomi", true);
    }

    public override void OnEventStay()
    {

    }

    public override void OnEventEnd()
    {

    }
}
