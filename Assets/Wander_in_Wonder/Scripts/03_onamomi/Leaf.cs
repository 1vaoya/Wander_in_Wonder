﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leaf : MonoBehaviour, TappedObject {

    public bool done = false;
    public bool isRemoved = false;

    [SerializeField] float distAngle = 120;
    [SerializeField] Transform pivot;

    public float angleRatio = 0;
    float currentAngleRatio = 0;

    AudioSource audioSource;

	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.RotateAround(pivot.position, Vector3.forward, (angleRatio - currentAngleRatio) * distAngle);
        currentAngleRatio = angleRatio;
        if (isRemoved && !done) {
            GetComponent<Animator>().SetTrigger("Remove");
            done = true;
        }
    }

    public void Remove ()
    {
        
    }

    public void OnTapBegan (Vector2 position) {
        Debug.Log(name);
        isRemoved = true;
        GetComponent<BoxCollider2D>().enabled = false;
    }

    public void OnTapMoved (Vector2 position, Vector2 deltaPosition) {
        
    }

    public void OnTapEnded (Vector2 position) {
        
    }

    public void Play () {
        audioSource.Play();
    }
}
