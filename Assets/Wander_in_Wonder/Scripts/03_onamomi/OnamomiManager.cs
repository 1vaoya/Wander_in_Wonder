﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnamomiManager : MonoBehaviour {

    public enum OnamomiState
    {
        idle,
        stick,
        removed
    }

    public Onamomi[] onamomi;
    public OnamomiState state = OnamomiState.idle;
    public Node node;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () { 
        switch (state) {
            case OnamomiState.idle:
                if (IsStick()) {
                    foreach (var e in node.edges) {
                        e.isPassable = false;
                    }
                    state = OnamomiState.stick;
                }
                break;
            case OnamomiState.stick:
                if (!IsStick())
                {
                    foreach (var e in node.edges)
                    {
                        e.isPassable = true;
                    }
                    state = OnamomiState.removed;
                }
                break;
            case OnamomiState.removed:
                break;
        }
	}

    bool IsStick () {
        bool ans = false;
        foreach (var o in onamomi) {
            ans |= o.state == Onamomi.OnamomiState.stick;
        }
        return ans;
    }

    public void StartStick () {
        foreach (var o in onamomi)
        {
            o.StartStick();
        }
    }
}
