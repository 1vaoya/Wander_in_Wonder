﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoManager : MonoBehaviour {

    public Animator animator;
    public Agent agent;
    public AnimationChanger animeChanger;
    public OnamomiManager oMgr;
    public LeafManager lMgr;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        animator.SetInteger("demo", 1);
	}
	
	// Update is called once per frame
	void Update () {
        if  (animator.GetInteger("demo") != 0) {
            animeChanger.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        } else {
            animeChanger.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }

        if (animator.GetInteger("demo") == 1 && lMgr.count == 1) {
            animator.SetInteger("demo", 2);
        }
	}

    public void StartOnamomi () {
        oMgr.StartStick();
    }
}
