﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Taiko : MonoBehaviour {

    public AudioSource au, yorokobi;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void MuteOff () {
        au.mute = false;
    }

    public void MuteOn () {
        au.mute = true;
    }

    public void Yorokobi () {
        yorokobi.Play();
    }
}
