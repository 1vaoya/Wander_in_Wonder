﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnpassableBranch : MonoBehaviour, TappedObject {

    AudioSource audioSource;

    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update () {

    }

    public void OnTapBegan (Vector2 position) {
        audioSource.PlayOneShot(audioSource.clip);
    }

    public void OnTapMoved (Vector2 position, Vector2 deltaPosition) {

    }

    public void OnTapEnded (Vector2 position) {

    }
}