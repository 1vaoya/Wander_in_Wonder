﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaredEvent : MyEvent {

    public DemoManager demo;

	// Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void OnEventStart()
    {
        demo.animator.SetInteger("demo", 5);
    }

    public override void OnEventStay()
    {

    }

    public override void OnEventEnd()
    {

    }
}
