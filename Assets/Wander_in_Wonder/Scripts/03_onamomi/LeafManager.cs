﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeafManager : MonoBehaviour, TappedObject {

    //public DemoManager demo;
    public int count = 0;
    public Leaf[] leaves;
    public TapSound sound;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (!GetComponent<BoxCollider2D>().enabled) return;
        bool done = true;
        foreach (var leaf in leaves) {
            done &= leaf.done;
            if (leaf.done) count = 1;
        }
        if (done) {
            GetComponent<BoxCollider2D>().enabled = false;
            sound.gameObject.SetActive(true);
        }
	}

    public void OnTapBegan (Vector2 position) {
        //if (count == 0) demo.GetComponent<Animator>().SetInteger("demo", 2);
        //if (count < leaves.Length) leaves[count].isRemoved = true;
        //if (count == leaves.Length - 1) GetComponent<BoxCollider2D>().enabled = false;
        //count++;
    }

    public void OnTapMoved (Vector2 position, Vector2 deltaPosition) {

    }

    public void OnTapEnded (Vector2 position) {

    }

    public bool IsDone () {
        bool done = true;
        foreach (var leaf in leaves)
        {
            done &= leaf.done;
        }
        return done;
    }
}
