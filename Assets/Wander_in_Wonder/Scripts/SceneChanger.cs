﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MyEvent {

    [SerializeField] string scene;

    public Node to;
    public Agent agent;

    public int time = 120;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
    }

    public override void OnEventStart()
    {
        StartCoroutine("Change");
        if (to != null)
        {
            Debug.Log("フレームアウトしまーす");
            Debug.Log(agent.StartMove(to));
        }
    }

    public override void OnEventStay()
    {

    }

    public override void OnEventEnd()
    {
        
    }

    public IEnumerator Change () {
        for (int i = 0; i < time; i++) {
            yield return null;
        }
        SceneManager.LoadSceneAsync(scene);
        //Resources.UnloadUnusedAssets();
        //System.GC.Collect();
    }
}
