﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Demo04 : MonoBehaviour {

    public Agent agent;
    public Animator animator;
    public ExAnimeChanger animator2;
    public AnimationChanger animeChanger;

    public Animator kinoko;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        animator.SetInteger("demo", 1);
        kinoko.SetTrigger("In");
	}

	// Update is called once per frame
	void Update () {
        if (animator.GetInteger("demo") != 0 || animator2.naname)
        {
            animeChanger.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }
        else
        {
            animeChanger.gameObject.GetComponent<SpriteRenderer>().enabled = true;
            if (animator.GetInteger("demo") == 0) {
                AfterJump();
            }
        }
	}

    void Jump () {
        transform.position = animeChanger.transform.position - new Vector3(0, 0.12f, 0);
    }

    void AfterJump () {
        transform.position = Vector3.zero;
    }

    public void flip (bool flipX) {
      GetComponent<SpriteRenderer>().flipX = flipX;
    }

    public void flipModosu () {
      GetComponent<SpriteRenderer>().flipX = false;

    }
}
