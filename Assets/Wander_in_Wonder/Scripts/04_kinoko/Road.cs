﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour {

    public int c = 0;
    public Node[] from;
    public Node[] to;

    public void Activate()
    {
        GetComponent<SpriteRenderer>().enabled = true;
        for (int i = 0; i < c; i++) {
            GraphManager.Instance.GetEdge(from[i], to[i]).isPassable = true;
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

}
