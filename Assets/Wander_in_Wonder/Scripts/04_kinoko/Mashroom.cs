﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mashroom : MonoBehaviour, TappedObject {

    public Agent agent;
    public Node node;
    public Demo04 demo;

    public Mashroom[] mashroom;
    public Road[] road;

    public bool init = false;
    public bool end = false;
    public Node endnode;

    Animator animator;

    public void Activate()
    {
        GetComponent<CircleCollider2D>().enabled = true;
        GetComponent<SpriteRenderer>().enabled = true;
    }

    public void AfterSpore () {
        foreach (var m in mashroom)
        {
            m.Activate();
        }
        foreach (var r in road)
        {
            r.Activate();
        }
    }

	// Use this for initialization
	void Start () {
        if (init) {
            GetComponent<SpriteRenderer>().enabled = true;
            GetComponent<CircleCollider2D>().enabled = true;
        };
        animator = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {

	}

    public void OnTapBegan(Vector2 position)
    {
        if (agent.currentNode == node && agent.state == Agent.State.idle) {
            if (!end) {
              demo.animator.SetInteger("demo", 2);
              demo.flip(agent.GetComponentInChildren<SpriteRenderer>().flipX);
            }
            else {
                demo.animator.SetInteger("demo", 3);
            }
            animator.SetTrigger("Spore");
            GetComponent<CircleCollider2D>().enabled = false;
        } else {
          GetComponents<AudioSource>()[1].Play();
            agent.StartMove(node);
        }
    }

    public void OnTapMoved(Vector2 position, Vector2 deltaPosition)
    {

    }

    public void OnTapEnded(Vector2 position)
    {

    }

    public SpriteRenderer pop;
    public void Pop () {
        pop.enabled = true;
    }

    public void Play()
    {
        GetComponent<AudioSource>().Play();
    }

    public void Warp () {
        agent.WarpTo(endnode);
    }
}
