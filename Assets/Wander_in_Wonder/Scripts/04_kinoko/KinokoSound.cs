﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KinokoSound : MonoBehaviour
{

    public AudioClip[] ashioto;
    public bool[] loop;
    public Animator demo;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (demo != null && demo.GetInteger("demo") == 0)
        {
            GetComponent<AudioSource>().enabled = true;
        }
        else if (demo != null)
        {
            GetComponent<AudioSource>().enabled = false;
        }
    }

    public void Play()
    {
        var id = Random.Range(0, ashioto.Length);
        GetComponent<AudioSource>().PlayOneShot(ashioto[id]);
    }

    public void PlayId(int id)
    {
        AudioSource au = GetComponent<AudioSource>();
        au.clip = ashioto[id];
        au.loop = loop[id];
        au.Play();
    }
}