﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[Serializable]
public class MovieList {
    public VideoClip clip;
    public bool loop;
    public MyEvent m_event;
    public Sprite frame;
}
