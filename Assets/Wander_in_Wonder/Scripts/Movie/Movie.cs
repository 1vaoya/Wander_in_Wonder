﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Movie : MonoBehaviour, TappedObject
{

    public enum Type
    {
        op,
        ed
    }

    VideoPlayer vp;
    SpriteRenderer renderer;
    CircleCollider2D collider;
    public MovieList[] list;
    MovieList info;
    int index = 0;
    bool isPreparing = false;
    bool isTapped = false;

    public Type type = Type.op;
    public Animator taphere;

    // Use this for initialization
    void Start()
    {
        vp = GetComponent<VideoPlayer>();
        renderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isPreparing && vp.frame < 5)
        {
            renderer.enabled = true;
        }
        else
        {
            renderer.enabled = false;
        }

        if (isPreparing && vp.frame >= 5)
        {
            isPreparing = false;
        }
        if (isTapped)
        {
            Next();
        }
        if (!isPreparing && !vp.isPlaying)
        {
            if (info != null && info.m_event != null) info.m_event.OnEventEnd();
            Next();
        }
        //Debug.Log(vp.frame);
        //Debug.Log(isPreparing);
        //Debug.Log(vp.isPlaying);
        switch (type)
        {
            case Type.op:
                {
                    Op(index, vp.frame);
                    break;
                }
            case Type.ed:
                {
                    Ed(index, vp.frame);
                    break;
                }
        }
    }

    bool Next()
    {
        if (index >= list.Length) return false;
        vp.Pause();
        info = list[index];
        if (info != null && info.m_event != null) info.m_event.OnEventStart();
        vp.clip = info.clip;
        vp.isLooping = info.loop;
        vp.Play();
        collider.enabled = vp.isLooping;
        isPreparing = true;
        renderer.sprite = info.frame;
        renderer.enabled = true;
        index++;
        return true;
    }

    public void OnTapBegan(Vector2 position)
    {
        if (vp.isLooping)
        {
            isTapped = true;
            GetComponents<AudioSource>()[1].Play();
            TapHereEnd();
        }
    }

    public void OnTapMoved(Vector2 position, Vector2 deltaPosition)
    {

    }

    public void OnTapEnded(Vector2 position)
    {

    }

    bool op1 = false;
    bool op2 = false;
    bool tap = false;
    void Op (int id, long frame) {
        //Debug.Log(id);
        //Debug.Log(frame);
        if (id == 1 && frame >= 728 && !op1) {
            op1 = true;
            GetComponent<AudioSource>().Play();
        }
        if (id == 2 && frame <= 200 && !tap) {
          TapHere();
          tap = true;
        }
        if (id == 3 && frame >= 281 && !op2) {
            op2 = true;
            Audio.Instance.GetComponent<AudioSource>().Play();
            Audio.Instance.GetComponent<AudioSource>().volume = 1;
        }
    }

    void Ed (int id, long frame) {

    }

    public void TapHere() {
      taphere.SetTrigger("Tap");
    }

    public void TapHereEnd() {
      taphere.SetTrigger("End");
    }

}
