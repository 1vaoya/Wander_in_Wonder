﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GodTouches;

public class Switcher : MonoBehaviour
{

    bool active = false;
    public GameObject[] objs;

    int counter = 0;

    // Use this for initialization
    void Start()
    {
        foreach (var obj in objs)
        {
            obj.gameObject.SetActive(active);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (3 <= Input.touchCount)
        {
            counter++;
            if (60 <= counter)
            {
                foreach (var obj in objs)
                {
                    obj.gameObject.SetActive(true);
                }
				counter = 0;
            }
        }
        else
        {
			counter = 0;
        }
    }

    public void SwitchScene(int scene)
    {
        string name = "07to01";
        switch (scene)
        {
            case 1:
                name = "07to01";
                break;
            case 2:
                name = "01to02";
                break;
            case 3:
                name = "02to03";
                break;
            case 4:
                name = "03to04";
                break;
            case 5:
                name = "04to05";
                break;
            case 6:
                name = "05to06";
                break;
            case 7:
                name = "06to07";
                break;
        }
        SceneManager.LoadSceneAsync(name);
    }

    public void Disable()
    {
        foreach (var obj in objs)
        {
            obj.gameObject.SetActive(false);
        }
    }
}
