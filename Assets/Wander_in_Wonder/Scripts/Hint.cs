﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hint : MonoBehaviour {

    // Use this for initialization
    public int interval = 150;
	void Start () {
        StartCoroutine("HintTimer");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator HintTimer () {
        while (true) {
            yield return new WaitForSeconds (interval);  
            GetComponent<Animator>().SetTrigger("Hint");
        }
    }
}
