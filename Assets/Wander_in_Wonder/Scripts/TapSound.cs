﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapSound : MonoBehaviour, TappedObject {

    public AudioClip[] ashioto;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

	public void OnTapBegan (Vector2 position) {
		Play();
	}

	public void OnTapMoved (Vector2 position, Vector2 deltaPosition) {

	}

	public void OnTapEnded (Vector2 position) {

	}

    public void Play()
    {
        var id = Random.Range(0, ashioto.Length);
        GetComponent<AudioSource>().PlayOneShot(ashioto[id]);
    }
}
