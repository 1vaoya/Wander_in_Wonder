﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chase : MonoBehaviour {

    [SerializeField] Transform target;
    [SerializeField] float limitLeft, limitRight;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        var p = target.position.x;

        if (p < limitLeft) {
            p = limitLeft;
        } else if (limitRight < p) {
            p = limitRight;
        }

        transform.position = new Vector3(p, transform.position.y, transform.position.z);
	}
}
