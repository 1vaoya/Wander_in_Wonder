﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Demo02 : MonoBehaviour {

    Animator animator;
    public AnimationChanger animeChanger;
    public string scene;
    public Animator tapguide;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        animator.SetInteger("demo", 1);
	}
	
	// Update is called once per frame
	void Update () {
        if (animator.GetInteger("demo") != 0)
        {
            animeChanger.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }
        else
        {
            animeChanger.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }
	}

    public void NextScene()
    {
        SceneManager.LoadScene(scene);
        //Resources.UnloadUnusedAssets();
        //System.GC.Collect();
    }

    public void TapGuideStart() {
        tapguide.SetTrigger("Tap");
    }
}
