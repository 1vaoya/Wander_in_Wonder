﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class seed : MonoBehaviour, TappedObject {

    public Agent agent;
    public Animator demo;
    public Node node;
    public string scene;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void OnTapBegan(Vector2 position) {
        if (agent.currentNode == node) {
            demo.SetInteger("demo", 2);
            gameObject.SetActive(false);
        } else {
            agent.StartMove(node);
        }
        Play();
    }

    public void OnTapMoved(Vector2 position, Vector2 deltaPosition) {
        
    }

    public void OnTapEnded(Vector2 position) {
        
    }

    public void Seed () {
    }

    public void NextScene () {
        SceneManager.LoadScene(scene);
        Resources.UnloadUnusedAssets();
        System.GC.Collect();
    }

    public void Play()
    {
        GetComponent<AudioSource>().Play();
    }
}
