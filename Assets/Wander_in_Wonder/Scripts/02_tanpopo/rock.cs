﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rock : MonoBehaviour, TappedObject {

    public Animator usagi;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTapBegan(Vector2 position)
    {
        usagi.SetTrigger("cantGo");
        GetComponent<AudioSource>().Play();
    }

    public void OnTapMoved(Vector2 position, Vector2 deltaPosition)
    {

    }

    public void OnTapEnded(Vector2 position)
    {

    }
}
