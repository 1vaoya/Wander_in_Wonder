﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tanpopo : MonoBehaviour, TappedObject {

    Animator animator;
    public GameObject seed;
    public Ads addedSeed;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();

        if (seed != null)
        {
            seed.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void OnTapBegan(Vector2 position) {
        animator.SetTrigger("Seed");
        if (addedSeed != null) addedSeed.Stop();
    }

    public void OnTapMoved(Vector2 position, Vector2 deltaPosition) {
        
    }

    public void OnTapEnded(Vector2 position) {
        
    }

    public void Seed () {
        if (seed != null) {
            seed.SetActive(true);
        }
    }

    public void Play () {
        GetComponent<AudioSource>().Play();
    }

}
