﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Usagi : MonoBehaviour
{
    public Animator demo;
    [Header("Speed")]
    [SerializeField]
    float walkSpeed = 0.1f;
    [SerializeField]
    float trotSpeed = 0.1f;
    [SerializeField]
    float runSpeed = 0.1f;

    GraphManager graphMgr;
    Agent agent;
    public AnimationChanger anime;
    public ExAnimeChanger ex;

    // アイドルアニメ
    public int idleTime
    {
        get; set;
    }

    // Use this for initialization
    void Start()
    {
        graphMgr = GraphManager.Instance;
        agent = GetComponent<Agent>();
        anime = GetComponentInChildren<AnimationChanger>();
        idleTime = 0;

        anime.SetSize(agent.currentNode.size);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (graphMgr.isUpdated)
        {
            graphMgr.isUpdated = false;
            if (!anime.IsBusy && agent.StartMove(graphMgr.targetNode))
            {
                var e = agent.GetEdge();
                if (e != null)
                {
                    anime.SetMoveType(e.type);
                    anime.SetMoveDir(e.dir);
                    anime.SearchAnimation((graphMgr.targetNode.position - agent.currentNode.position).normalized);
                }

            }
        }

        if (agent.state == Agent.State.idle && demo.GetInteger("demo") == 0 && !(ex != null && ex.dance)) idleTime++;
        else
        {
            idleTime = 0;
        }
        if (idleTime == 300)
        {
            anime.IdleChanger();
            idleTime = 0;
        }

        agent.IsMovable = anime.IsMovable;
    }

    public void OnMoveStart()
    {
        anime.StartMove();
    }

    public void OnMoving()
    {
    }

    public void OnMoveToNext()
    {
        var e = agent.GetEdge();
        var n = agent.currentNode;
        SetSpeed(e.type, n.size);
        anime.SetMoveType(e.type);
        anime.SetMoveDir(e.dir);
        anime.SetLayer(n.sortingLayer, n.orderInLayer);
        anime.SetSize(e.to.size);
    }

    public void OnMoveEnd()
    {
        anime.StopMove();
    }

    void SetSpeed(MoveType type, float ratio = 1)
    {
        switch (type)
        {
            case MoveType.walk:
                agent.Speed = walkSpeed * ratio;
                break;
            case MoveType.trot:
                agent.Speed = trotSpeed * ratio;
                break;
            case MoveType.run:
                agent.Speed = runSpeed * ratio;
                break;
            case MoveType.jump:
                agent.Speed = agent.GetEdge().length / 2.0f * 24 / 60;
                anime.SetMovable(0);
                break;
            default:
                agent.Speed = walkSpeed * ratio;
                break;
        }
    }

}
