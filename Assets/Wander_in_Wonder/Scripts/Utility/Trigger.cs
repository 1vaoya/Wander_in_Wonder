﻿using UnityEngine;
using System;

[Serializable]
public class Trigger {
    [SerializeField]
    bool value = false;
    public bool Value {
        get { return value; }
        set { this.value = value; }
    }

    public void Activate () {
        if (value) {
            Debug.LogWarning("アクティブのフラグを再度アクティブにしようとしました");
        } else {
            value = true;
        }
    }

    public void Complete () {
        if (!value) {
            Debug.LogWarning("非アクティブのフラグを完了しようとしました");
        } else {
            value = false;
        }
    }
}
