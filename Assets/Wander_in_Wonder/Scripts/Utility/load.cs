﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class load : MonoBehaviour {

    [SerializeField] string scene;

	// Use this for initialization
	void Start () {
        SceneManager.LoadSceneAsync(scene);
	}
	
	// Update is called once per frame
	void Update () {
        //Resources.UnloadUnusedAssets();
        //System.GC.Collect();
	}
}
