﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChikaChika : MonoBehaviour {

    public SpriteRenderer[] sprites;
    public int[] timers;

    public bool chika = false;

    int counter = 0;

	// Use this for initialization
	void Start () {
        Sayonara();
	}
	
	// Update is called once per frame
	void Update () {
        counter++;
        if (chika && counter % 5 == 0) {
            counter = 0;
            foreach(var s in sprites)
            {
                if (Random.value > 0.3f) {
                    s.enabled = true;
                } else {
                    s.enabled = false;
                }
            }
        }
	}

    public void Hello () {
        StartCoroutine("Hey");
        foreach (var s in sprites)
        {
            //s.enabled = true;
        }
    }

    public void WakeUp () {
        foreach (var s in sprites)
        {
            var dance = s.GetComponent<Dance>();
            if (dance == null) {
                s.enabled = true;
            } else {
                dance.WakeUp();
            }
        }
    }

    public void Sayonara () {
        foreach (var s in sprites)
        {
            var dance = s.GetComponent<Dance>();
            if (dance == null)
            {
                s.enabled = false;
            }
            else
            {
                dance.Vanish();
            }
        }
    }

    public IEnumerator Hey() {
        int c = 0, timer = 0;
        while (true) {
            for (int i = 0; i < sprites.Length; i++) {
                if (timer == timers[i]) {
                    sprites[i].enabled = true;
                    c++;
                }
            }
            timer++;
            if (c >= sprites.Length) yield break;
            yield return null;
        }
    }
}
