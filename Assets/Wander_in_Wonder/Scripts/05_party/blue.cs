﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blue : MonoBehaviour {

    public Agent agent;
    public Usagi usagi;
    public Demo05 demo;
    public GameObject normal;

    public AnimationChanger animator;
    public ExAnimeChanger animator2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Activate () {
        usagi.anime = animator;
        demo.animeChanger = animator;
        demo.animator2 = animator2;
        normal.SetActive(false);
        gameObject.SetActive(true);

        //animator2.transform.localScale = animator.transform.localScale;

        animator.SetSize(agent.currentNode.size);
        //animator.SetLayer(agent.currentNode.sortingLayer, agent.currentNode.orderInLayer);
    }
}
