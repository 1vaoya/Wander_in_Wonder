﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Demo05 : MonoBehaviour {

    public Agent agent;

    public Animator animator;
    public ExAnimeChanger animator2;
    public AnimationChanger animeChanger;

    public Animator michi;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        animator.SetInteger("demo", 1);
        michi.SetTrigger("In");
	}

    // Update is called once per frame
    void LateUpdate () {
        if (animator.GetInteger("demo") != 0 || animator2.naname || animator2.dance)
        {
            animeChanger.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }
        else
        {
            animeChanger.gameObject.GetComponent<SpriteRenderer>().enabled = true;
            if (animator.GetInteger("demo") == 0) {
                AfterJump();
            }
        }
	}

    void Jump () {
        transform.position = animeChanger.transform.position - new Vector3(0, 0.12f, 0);
    }

    void AfterJump () {
        transform.position = Vector3.zero;
    }
}
