﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dance : MonoBehaviour, TappedObject {

    public bool dance = false;
    public GameObject dancer;

    public Dance link;

	// Use this for initialization
	void Start () {
        dancer.GetComponent<SpriteRenderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTapBegan(Vector2 position)
    {
        //GetComponent<Animator>().SetTrigger("Dance");
        DanceStart();
        if (link != null) link.DanceStart();
    }

    public void OnTapMoved(Vector2 position, Vector2 deltaPosition)
    {

    }

    public void OnTapEnded(Vector2 position)
    {

    }

    bool stop = false;
    public void Play () {
        //if (!GetComponent<AudioSource>().isPlaying && !stop)GetComponent<AudioSource>().Play();
        dancer.GetComponent<AudioSource>().mute = false;
    }

    public void Stop() {
        //dancer.GetComponent<AudioSource>().Stop();
        dancer.GetComponent<AudioSource>().mute = true;
        stop = true;
    }

    public void WakeUp () {
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<BoxCollider2D>().enabled = true;
    }

    public void DanceStart () {
        dance = true;
        GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
        dancer.GetComponent<SpriteRenderer>().enabled = true;
        Play();
    }

    public void Vanish() {
        GetComponent<SpriteRenderer>().enabled = false;
        dancer.GetComponent<SpriteRenderer>().enabled = false;
        Stop();
    }
}
