﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guru : MonoBehaviour
{

    public Dance[] dance;
    public Guruguru[] guruguru;
    public Animator blue;
    public Animator anime;
    public ChikaChika chika;

    public int counter = 0;
    public int timeToMax = 600;
    public int timeToEnd = 600;

    bool gurugury = false;
    bool pika = false;
    public bool afterPika = false;

    public AudioSource refr;
    public AudioSource bgm;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (IsDance() && !gurugury && !pika) {
            counter++;
            if (counter > timeToMax) {
                foreach (var g in guruguru) {
                    g.Gurugurw();
                    gurugury = true;
                    counter = 0;
                }
                anime.SetBool("dance", true);
                GetComponent<Ashioto>().PlayId(0);
                StartCoroutine("Stop");
                chika.chika = true;
            }
        }
        if (gurugury && !pika) {
            counter++;
            if (counter > timeToEnd) {
                gurugury = false;
                pika = true;
                GetComponent<Animator>().SetTrigger("Guru");
                blue.SetTrigger("Pika");
                foreach (var g in guruguru)
                {
                    g.Stop();
                    counter = 0;
                }
            }
        }
        if (pika) {
            
        }
	}

    bool IsDance () {
        bool ans = true;
        foreach (var d in dance) {
            ans &= d.dance;
        }
        return ans;
    }

    public void AfterPika () {
        afterPika = true; 
    }

    IEnumerator Stop () {
        for (int i = 0; i < 60 * 7; i++) {
            yield return null;
        }
        while (true) {
            Debug.Log(refr.time);
            if (refr.time < 1) {
                bgm.Play();
                break;
            }
            yield return null;
        }
        foreach (var d in dance)
        {
            d.Stop();
        }
        yield break;
    }

    public void KirakiraSound () {
        GetComponents<AudioSource>()[1].Play();
    }
}
