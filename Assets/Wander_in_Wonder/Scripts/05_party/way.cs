﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class way : MonoBehaviour {

    public Node from, to;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Way () {
        GetComponent<Animator>().SetTrigger("Way");
    }

    public void AfterWay () {
        GraphManager.Instance.GetEdge(from, to).isPassable = true;
    }
}
