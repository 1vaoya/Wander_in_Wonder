﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class King : MyEvent {

    public Agent agent;
    public ChikaChika[] chikas;
    Animator animator;

    public AnimationChanger anime;

    public Node helloNode;
    bool hellozumi = false;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (agent.currentNode == helloNode && !hellozumi) {
            animator.SetTrigger("Hello");
        }
	}

    public void Hello () {
        anime.SetLayer("Default", 0);
        hellozumi = true;
    }

    public void HelloBaibai () {
        foreach (var c in chikas)
        {
            c.WakeUp();
        }
    }

    public void ChikaStart () {
        foreach (var c in chikas)
        {
            c.chika = true;
        }
    }

    public void ChikaEnd () {
        foreach (var c in chikas)
        {
            c.chika = false;
        }
    }

    public void Sayonara () {
        foreach (var c in chikas)
        {
            c.Sayonara();
        }
    }
}
