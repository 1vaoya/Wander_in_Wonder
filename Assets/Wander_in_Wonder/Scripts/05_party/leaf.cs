﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class leaf : MonoBehaviour, TappedObject {

    public Agent agent;
    public Node node;

    public Animator demo;
    public Animator blue;

    public Guru guru;
    public way waay;

    public blue blu;

    public ChikaChika ao, right, left, center;

    Animator animator;

    public AudioSource bgm;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void After() {
        waay.Way();
        blu.Activate();
        right.Sayonara();
        left.Sayonara();
        center.Sayonara();
        ao.chika = false;
        ao.WakeUp();
        bgm.mute = true;
    }

    public void OnTapBegan(Vector2 position)
    {
        if (agent.currentNode == node && guru.afterPika)
        {
            demo.SetInteger("demo", 3);
            animator.SetTrigger("Get");
            blue.SetTrigger("Get");
            GetComponent<BoxCollider2D>().enabled = false;
        }
        else if (guru.afterPika)
        {
            agent.StartMove(node);
            var a = GetComponent<AudioSource>();
            if (!a.isPlaying) a.Play();
        }
    }

    public void OnTapMoved(Vector2 position, Vector2 deltaPosition)
    {

    }

    public void OnTapEnded(Vector2 position)
    {

    }
}
