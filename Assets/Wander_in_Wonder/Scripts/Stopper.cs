﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stopper : MonoBehaviour {

    public Agent agent;
    public Node node;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (agent.currentNode == node) {
            StopBGM();
        }
	}

    bool stop = false;
    public void StopBGM()
    {
        //Audio.Instance.GetComponent<AudioSource>().Stop();
        if (!stop)
        {
            stop = true;
            StartCoroutine("FadeOut");
        }
    }

    IEnumerator FadeOut()
    {
        for (int i = 0; i < 300; i++)
        {
            Audio.Instance.GetComponent<AudioSource>().volume = (300 - i) / 300f;
            yield return null;
        }
        Audio.Instance.GetComponent<AudioSource>().Stop();
    }
}
