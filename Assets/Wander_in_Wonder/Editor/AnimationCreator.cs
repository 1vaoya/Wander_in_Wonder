﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;

[Serializable]
public class AnimationDataContainer
{
    public AnimationData[] data;
}

[Serializable]
public class AnimationData
{
    public string name;
    public float x;
    public float y;
    public bool key;
    public int order;
}

public class AnimationCreator : MonoBehaviour
{
    [MenuItem("Tools/Create Animation &a")]
    public static void CreateAnimations()
    {
        foreach (var guid in Selection.assetGUIDs)
        {
            CreateAnimation(AssetDatabase.GUIDToAssetPath(guid));
        }
    }

    static void CreateAnimation(string path)
    {
        // アニメーションデータの取得
        TextAsset AnimationDataContainerAsset = (TextAsset)AssetDatabase.LoadAssetAtPath(path + "/positions.json", typeof(TextAsset));
        if (AnimationDataContainerAsset == null)
        {
            return;
        }
        else
        {
            Debug.Log("Animation Data is Found in " + path);
        }
        string json = AnimationDataContainerAsset.ToString();
        AnimationData[] datalist = JsonUtility.FromJson<AnimationDataContainer>(json).data;

        // アニメーションクリップの作成
        var clip = new AnimationClip();
        clip.frameRate = 24;

        // スプライトのアニメーションを追加
        var spriteBinding = new EditorCurveBinding();
        spriteBinding.path = null;
        spriteBinding.type = typeof(SpriteRenderer);
        spriteBinding.propertyName = "m_Sprite";

        int size = 0;
        for (int i = 0; i < datalist.Length; i++)
        {
            if (datalist[i].key)
            {
                size++;
            }
        }
        var keyframes = new ObjectReferenceKeyframe[size + 1];

        int index = 0;
        for (int i = 0; i < datalist.Length; i++)
        {
            if (datalist[i].key)
            {
                var frame = new ObjectReferenceKeyframe();
                frame.time = i / 24.0f;
                frame.value = FindSpriteObject(path, datalist[i].name);
                keyframes[index] = frame;
                index++;
            }
            else if (i == datalist.Length - 1)
            {
                var frame = new ObjectReferenceKeyframe();
                frame.time = i / 24.0f;
                frame.value = keyframes[index - 1].value;
                keyframes[index] = frame;
            }
        }

        AnimationUtility.SetObjectReferenceCurve(clip, spriteBinding, keyframes);

        // ポジションのアニメーションを追加
        var curveX = new AnimationCurve();
        var curveY = new AnimationCurve();

        index = 0;
        for (int i = 0; i < datalist.Length; i++)
        {
            float pixelsPerUnit = 100;
            if (datalist[i].key)
            {
                Sprite sprite = (Sprite)FindSpriteObject(path, datalist[i].name);
                pixelsPerUnit = sprite.pixelsPerUnit;
                float time = i / 24.0f;
                float x = datalist[i].x / pixelsPerUnit;
                float y = datalist[i].y / pixelsPerUnit;
                curveX.AddKey(time, x);
                curveY.AddKey(time, y);
            }
            else if (i == datalist.Length - 1)
            {
                float time = i / 24.0f;
                float x = datalist[i].x / pixelsPerUnit;
                float y = datalist[i].y / pixelsPerUnit;
                curveX.AddKey(time, x);
                curveY.AddKey(time, y);
            }
        }

        // デフォルトでは位置が滑らかに変化するので離散的に変化するように設定
        for (int i = 0; i < curveX.length; i++)
        {
            AnimationUtility.SetKeyRightTangentMode(curveX, i, AnimationUtility.TangentMode.Constant);
            AnimationUtility.SetKeyRightTangentMode(curveY, i, AnimationUtility.TangentMode.Constant);
        }

        clip.SetCurve("", typeof(Transform), "localPosition.x", curveX);
        clip.SetCurve("", typeof(Transform), "localPosition.y", curveY);

        // アセットの保存
        AssetDatabase.CreateAsset(clip, AssetDatabase.GenerateUniqueAssetPath(path + "/" + System.IO.Path.GetFileName(path)) + ".anim");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    static string GetFileNameFromGUID(string guid)
    {
        return System.IO.Path.GetFileName(AssetDatabase.GUIDToAssetPath(guid));
    }

    static UnityEngine.Object GetObjectFromGUID(string guid, Type type)
    {
        return AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), type);
    }

    static UnityEngine.Object FindSpriteObject(string path, string name)
    {
        string[] guids = AssetDatabase.FindAssets("t:Sprite " + name, new string[] { path });
        if (guids.Length != 1)
        {
            Debug.LogError("Failed to load sprite");
            return null;
        }
        string guid = guids[0];
        return GetObjectFromGUID(guid, typeof(Sprite));
    }
}
