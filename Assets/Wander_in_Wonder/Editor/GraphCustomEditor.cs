﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GraphManager))]//拡張するクラスを指定
public class GraphCustomEditor : Editor {

    public override void OnInspectorGUI () {
        base.OnInspectorGUI();

        var gm = target as GraphManager;

        if (GUILayout.Button("Build Graph")) {
            gm.BuildGraph();
        }
    }

}