using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;

public class LandscapeCreator : MonoBehaviour
{
    [MenuItem("Tools/Create Landscape &l")]
    public static void CreateLandscapes()
    {
        foreach (var guid in Selection.assetGUIDs)
        {
            CreateLandscape(AssetDatabase.GUIDToAssetPath(guid));
        }
    }

    static void CreateLandscape(string path)
    {
        // アニメーションデータの取得
        TextAsset LocationDataContainerAsset = (TextAsset)AssetDatabase.LoadAssetAtPath(path + "/positions.json", typeof(TextAsset));
        if (LocationDataContainerAsset == null)
        {
            return;
        }
        else
        {
            Debug.Log("Location Data is Found in " + path);
        }
        string json = LocationDataContainerAsset.ToString();
        AnimationData[] datalist = JsonUtility.FromJson<AnimationDataContainer>(json).data;

        foreach (var data in datalist)
        {
            string basename = System.IO.Path.GetFileNameWithoutExtension(data.name);
            Sprite sprite = (Sprite)FindSpriteObject(path, data.name);
            float pixelsPerUnit = sprite.pixelsPerUnit;

            GameObject obj = new GameObject(basename);
            obj.transform.position = new Vector3(data.x / pixelsPerUnit, data.y / pixelsPerUnit, 0);

            var renderer = obj.AddComponent<SpriteRenderer>();
            renderer.sprite = sprite;
            renderer.sortingOrder = -data.order;
        }

    }

    static UnityEngine.Object GetObjectFromGUID(string guid, Type type)
    {
        return AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), type);
    }

    static UnityEngine.Object FindSpriteObject(string path, string name)
    {
        string[] guids = AssetDatabase.FindAssets("t:Sprite " + name, new string[] { path });
        if (guids.Length != 1)
        {
            Debug.LogError("Failed to load sprite");
            Debug.LogError(guids.Length);
            return null;
        }
        string guid = guids[0];
        return GetObjectFromGUID(guid, typeof(Sprite));
    }
}
