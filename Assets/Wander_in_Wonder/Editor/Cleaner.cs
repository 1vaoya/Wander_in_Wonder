﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;

public class Cleaner : MonoBehaviour
{

    [MenuItem("Tools/Clean Textures &c")]
    public static void Clean()
    {
        foreach (var guid in Selection.assetGUIDs)
        {
            MakeAnimationClip(AssetDatabase.GUIDToAssetPath(guid));
        }
    }

    static void MakeAnimationClip(string path)
    {
        // スプライトの読み込み
        string[] paths = { path };
        var sprites = AssetDatabase.FindAssets("t:Sprite", paths);
        if (sprites == null)
        {
            Debug.Log("スプライトがねぇよ");
            return;
        }

        // アニメーションクリップの作成
        for (int i = 0; i < sprites.Length; i++)
        {
            string filename = System.IO.Path.GetFileName(AssetDatabase.GUIDToAssetPath(sprites[i]));
            if (filename.IndexOf('!') == -1 && i != sprites.Length - 1)
            {
                AssetDatabase.MoveAssetToTrash(AssetDatabase.GUIDToAssetPath(sprites[i]));
            }
        }

        // アセットの保存
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}
